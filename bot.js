const request = require("request");
const setCookie = require("set-cookie-parser");
const randomstring = require("randomstring");

const Chess = require("chess").Chess;
const Engine = require("node-uci").Engine;

import {
  COLOR,
  MOVE_INPUT_MODE,
  MARKER_TYPE,
  Chessboard
} from "./js/Chessboard.js";

var DEBUG = 0;

const board_styles = ["default", "green", "blue"];

var board_config = {
  position: "start",
  responsive: true,
  moveInputMode: MOVE_INPUT_MODE.viewOnly,
  orientation: COLOR.black,
  style: {
    cssClass: board_styles[1],
    showCoordinates: true,
    showBorder: true
  }
};

var board = new Chessboard(
  document.getElementById("board"),
  board_config,
  board => {
    board.enableContextInput(event => {
      const markersOnSquare = board.getMarkers(
        event.square,
        MARKER_TYPE.emphasize
      );
      if (markersOnSquare.length > 0) {
        board.removeMarkers(event.square, MARKER_TYPE.emphasize);
      } else {
        board.addMarker(event.square);
      }
    });
  }
);

const x = ["a", "b", "c", "d", "e", "f", "g", "h"];
const y = [8, 7, 6, 5, 4, 3, 2, 1];
const promotion_options = ["q", "n", "b", "r"];
const CHAT_BUFFER = 100;

var chess = new Chess();

var turn = 0;
var pieces_color = COLOR.white;
var section_players = [];
var room_players = [];
var current_room = 0;
var current_section = "";
var current_seat = -1;
var room_players = [];
var room_seats = [{ free: false, name: "" }, { free: false, name: "" }];
var auto_start = true;
var bot_start_time = Date.now();
var move_start_time = Date.now();
var depth = 18;
var cv = 0;

var current_view = null;
var login_view = document.getElementById("login_view");
var lobby_view = document.getElementById("lobby");
var players_table_element = document.getElementById("user_table");
var game_view = document.getElementById("room_view");
var chat_div = document.getElementById("chat");
var chat_list = chat_div.children[0];

var ws = null;

var engine_name = "";
const engine = new Engine("asmFishW_2018-05-29_base.exe");

function getBotUptime() {
  return Date.now() - bot_start_time;
}

async function initEngine() {
  await engine.init();
  await engine.isready();
  //await engine.write('setoption name LogFile value C:\\Users\\root\\Desktop\\chess_test\\log.txt');
  await engine.sendCmd("setoption name Threads value 4");
  await engine.sendCmd("setoption name OwnBook value true");
  await engine.write(
    "setoption name BookFile value C:\\Users\\root\\Desktop\\chess_test\\books\\Cerebellum_Light_Poly.bin"
  );
  await engine.sendCmd("setoption name BestBookMove value true");
  await engine.sendCmd("setoption name Hash value 512");
  engine_name = engine.id.name.split("_")[0];
}

function removeFromArray(array, index) {
  return array.slice(0, index).concat(array.slice(index + 1));
}

function getEloColor(n) {
  if (n < 1199) {
    return "blue";
  }
  if (n < 1499) {
    return "green";
  }
  if (n < 1799) {
    return "yellow";
  }
  if (n < 2099) {
    return "orange";
  }
  return "red";
}

function sendCommand(json) {
  if (DEBUG) {
    console.log("<-- ", json);
  }
  ws.send(json);
}

function updatePlayerTable() {
  section_players.sort(function(a, b) {
    return a.elo - b.elo;
  });
  players_table_element.innerHTML = "";
  let el = document.getElementById("users");
  el.children[0].innerText = "Users: " + section_players.length;
  section_players.forEach(e => {
    addPlayerToTable(e);
  });
}

function handleSectionsInfo(data) {
  current_section = data.i[1] + 1 + "00";
  document.getElementById("users").children[1].innerText =
    "Section: " + current_section;
  let str = data.s[0].split("\n");
  let sel = document.getElementById("sections_table");
  sel.innerHTML = "";
  str.forEach(element => {
    let opt = document.createElement("option");
    opt.innerHTML = element;
    sel.appendChild(opt);
  });
}

function addPlayerToTable(data) {
  let r = players_table_element.insertRow(0);
  r.insertCell(0).innerHTML = data.name;
  r.insertCell(1).innerHTML = data.room;
  r.insertCell(2).innerHTML =
    data.elo +
    '<i style="font-style:normal;" text="' +
    getEloColor(data.elo) +
    '"> ⬤</i>';
}

function handlePlayerList(data) {
  let c = 3;
  section_players = [];
  if (data.s) {
    data.s.forEach(element => {
      section_players.push({
        name: element,
        n: data.i[c],
        room: data.i[c + 1],
        elo: data.i[c + 2]
      });
      c += 3;
    });
  }
}

function handlePlayerLeaveSection(data) {
  section_players.forEach(e => {
    if (e.name == data.s[0]) {
      let i = section_players.indexOf(e);
      section_players = removeFromArray(section_players, i);
    }
  });
  updatePlayerTable();
}

function handlePlayerJoinSection(data) {
  let found = false;
  section_players.forEach(a => {
    if (a.name == data.s[0]) {
      a = { name: data.s[0], n: data.i[1], room: data.i[2], elo: data.i[3] };
      found = true;
    }
  });
  if (!found) {
    section_players.push({
      name: data.s[0],
      n: data.i[1],
      room: data.i[2],
      elo: data.i[3]
    });
  }
  updatePlayerTable();
}

function handleRoomInfo(data) {
  if (data.i[1] == current_room) {
    for (let i = 0; i < 2; i++) {
      if (data.i[3 + i]) {
        room_seats[i].free = false;
        room_seats[i].name = data.s[i + 1];
      } else {
        room_seats[i].free = true;
        room_seats[i].name = "";
      }
    }
    room_seats.some(e => {
      if (e.free) {
        return false;
      } else {
        if (auto_start) {
          startGame();
        }
      }
    });
  }
}

function handleRoomCreationInfo(data) {
  current_room = data.i[1];
}

async function getBestMove() {
  await engine.position(chess.fen());
  const result = await engine.go({ depth: depth });
  return result.bestmove;
}
var last_move = {};
//sendCommand(JSON.stringify());
function sendBestMove() {
  move_start_time = Date.now();
  getBestMove().then(res => {
    let m = chess.move(res, { sloppy: true });
    let pos = [x.indexOf(m.from[0]), y.indexOf(parseInt(m.from[1]))];
    let dest = [x.indexOf(m.to[0]), y.indexOf(parseInt(m.to[1]))];
    let r = ((dest[1] * 8 + dest[0]) * 8 + pos[1]) * 8 + pos[0];
    if (m.promotion) {
      r = (promotion_options.indexOf(m.promotion) + 1) * 4096 + r;
    }

    let time = Math.floor((Date.now() - move_start_time) / 100);
    if (!time) {
      time = 1;
    }
    last_move = { i: [92, current_room, 1, r, time] };
    sendCommand(JSON.stringify(last_move));
    updateBoardView();
  });
}

function handleStartGame(data) {
  chess = new Chess();
  if (data.i.length > 2) {
    pieces_color = COLOR.black;
    board.setOrientation(pieces_color);
  } else {
    pieces_color = COLOR.white;
    board.setOrientation(pieces_color);
  }
  board.setPosition("start");
}

function handleSeatInRoom(data) {
  current_seat = data.i[4];
}

function updateBoardView() {
  board.setPosition(chess.fen(), false);
}

function handlePossibleMoves(data) {
  turn = data.i[3];
  if (turn > -1) {
    if (turn == current_seat) {
      sendBestMove();
      updateBoardView();
    }
  }
}

function handleEnemyMove(data) {
  let move = data.s[0];
  if (turn > -1) {
    if (turn != current_seat) {
      chess.move(move);
      updateBoardView();
    }
  }
}

function setRoomSetings() {
  //sample
  sendCommand(JSON.stringify({ i: [82, 1800, 3], s: ["tg"] })); //match time 3m
  sendCommand(JSON.stringify({ i: [82, 1800, 1], s: ["ud"] })); //no undo
}

function sendKeepAlive(data) {
  sendCommand('{"i":[2]}');
}

function leaveRoom() {
  game_view.hidden = true;
  lobby_view.hidden = false;
  sendCommand(JSON.stringify({ i: [73, current_room] }));
  current_room = 0;
}

function checkPlayer(name) {
  sendCommand(JSON.stringify({ i: [61], s: [name] }));
}

function joinSections(section) {
  let d = section.split(" ")[0];
  let s = "/join " + d;
  sendCommand(JSON.stringify({ i: [20], s: [s] }));
}

function receiveChatMessage(data) {
  handleChat(data.s[0]);
}

function sendChatMessage(message) {
  sendCommand(JSON.stringify({ i: [81, current_room], s: [message] }));
}

function joinRoom(room) {
  sendCommand(JSON.stringify({ i: [72, room] }));
  current_room = room;
}

function createRoom() {
  lobby_view.hidden = true;
  game_view.hidden = false;
  sendCommand(JSON.stringify({ i: [71] }));
}

function takeSeat(seat) {
  sendCommand(JSON.stringify({ i: [83, current_room, seat] }));
}

function startGame() {
  sendCommand(JSON.stringify({ i: [85, current_room] }));
}

function logout() {
  ws.close();
  current_view.hidden = true;
  login_view.hidden = false;
}

function handlePlayerJoinRoom(data) {
  for (let i = 0; i < section_players.length; i++) {
    if (section_players[i].name == data.s[0]) {
      room_players.push(section_players[i]);
      break;
    }
  }
}

function handlePlayerLeaveRoom(data) {
  for (let i = 0; i < room_players.length; i++) {
    if (room_players[i].name == data.s[0]) {
      room_players = removeFromArray(room_players, i);
      break;
    }
  }
}

function handleCommand(command, data) {
  let f = commands[command];
  if (f) {
    f(data);
  }
}

function initWS(session_id) {
  ws = new WebSocket("wss://x.kurnik.pl:17003/ws/");
  let ws_login_payload = {
    i: [17010],
    s: [
      session_id + "+395882724308244431||",
      "en",
      "b",
      "",
      navigator.userAgent,
      "/" + Date.now() + "/1",
      "w",
      "screen:1366x768 1",
      "ref:https://www.kurnik.pl/szachy/",
      "ver:129"
    ]
  };
  ws.onopen = function() {
    ws.onmessage = function(m) {
      let d = JSON.parse(m.data);
      let command = d.i[0];
      handleCommand(command, d);
      if (DEBUG) {
        console.log("--> ", m);
      }
    };
    sendCommand(JSON.stringify(ws_login_payload)); //login
    lobby_view.hidden = false;
    document.getElementById("sections").hidden = false;
    document.getElementById("login").hidden = true;
  };
  ws.onclose = function(r) {
    console.log(r);
    logout();
  };
}

function login(user) {
  login_view.children[2].hidden = true;

  let login_headers = {
    "User-Agent": navigator.userAgent,
    "Save-Data": "on"
  };
  if (user) {
    let l = document.getElementById("login_input").value;
    let p = document.getElementById("password_input").value;

    let login_payload = { cc: "0", id: l, pw: p };
    request.post(
      {
        url: "https://www.kurnik.pl/login.phtml",
        form: login_payload,
        headers: login_headers
      },
      function(error, response, body) {
        let cookies = setCookie.parse(response, {
          decodeValues: true
        });
        if (cookies[1]) {
          login_view.hidden = true;
          initWS(cookies[1].value.split(":")[0]);
        } else {
          login_view.children[2].hidden = false;
        }
      }
    );
  } else {
    initWS(
      randomstring.generate({
        length: 16,
        charset: "alphanumeric",
        capitalization: "lowercase"
      })
    );
    login_view.hidden = true;
  }
}

function handleGoSectionButton() {
  let e = document.getElementById("sections_table");
  joinSections(e.value);
}

function handleChat(message) {
  let e = document.createElement("li");
  if (message[0] == "*" && message[1] == "*") {
    let m = "+ " + message.substring(3, message.length);
    e.innerText += m;
  } else {
    let i = message.indexOf(":");
    let s = message.substring(0, i);
    e.innerHTML = "<b>" + s + "</b>";
    e.innerHTML += ": " + message.substring(i + 1, message.length);
  }
  chat_list.appendChild(e);
  chat_div.scrollTop = chat_div.scrollHeight;
  if (chat_list.childElementCount > CHAT_BUFFER) {
    chat_list.removeChild(chat_list.firstElementChild);
  }
}

var commands = {
  1: sendKeepAlive,
  24: handlePlayerLeaveSection,
  25: handlePlayerJoinSection,
  27: handlePlayerList,
  32: handleSectionsInfo,
  70: handleRoomInfo,
  73: handleRoomCreationInfo,
  81: receiveChatMessage,
  84: handlePlayerJoinRoom,
  85: handlePlayerLeaveRoom,
  88: handleSeatInRoom,
  90: handlePossibleMoves,
  91: handleStartGame,
  92: handleEnemyMove
};

function init() {
  initEngine();

  document.getElementById("login").addEventListener("click", function() {
    login(true);
  });
  document.getElementById("guest").addEventListener("click", function() {
    login(false);
  });
  document.getElementById("leave_room").addEventListener("click", leaveRoom);
  document
    .getElementById("go_section")
    .addEventListener("click", handleGoSectionButton);
  document.getElementById("create_room").addEventListener("click", createRoom);
  document.getElementById("seat_0").addEventListener("click", function() {
    takeSeat(0);
  });
  document
    .getElementById("chat_message")
    .addEventListener("keyup", function(event) {
      event.preventDefault();
      if (event.keyCode === 13) {
        let m = document.getElementById("chat_message").value;
        sendChatMessage(m);
        document.getElementById("chat_message").value = "";
      }
    });
}

init();
