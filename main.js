const electron = require('electron');
const url = require('url');
const path = require('path');

const {app, BrowserWindow, globalShortcut} = electron;

let mainWindow;

app.on('ready', function(){
	mainWindow = new BrowserWindow({
		width: 1280,
		height: 720
	});
	mainWindow.loadURL(url.format({
		pathname: path.join(__dirname, 'index.html'),
		protocol: 'file:',
		slashes: true
	}))
	globalShortcut.register('Ctrl+D', () => {
		mainWindow.toggleDevTools();
	})
	globalShortcut.register('F5', () => {
		mainWindow.reload();
	})
	mainWindow.setMenu(null);
});
